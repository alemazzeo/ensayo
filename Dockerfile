FROM python:3.8-slim
RUN pip install poetry
WORKDIR /code/
COPY pyproject.toml poetry.lock /code/
RUN poetry config virtualenvs.create false && \
    poetry install
COPY . /code/
RUN poetry install

ENTRYPOINT ["my-script"]
CMD ["--help"]